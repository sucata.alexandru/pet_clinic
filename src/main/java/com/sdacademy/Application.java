package com.sdacademy;

import com.sdacademy.consult.ConsultEntity;
import com.sdacademy.consult.ConsultEntityRepositoryImpl;
import com.sdacademy.owners.OwnerEntity;
import com.sdacademy.owners.OwnerEntityRepositoryImpl;
import com.sdacademy.owners.OwnerRepository;
import com.sdacademy.pets.PetsEntity;
import com.sdacademy.pets.PetsEntityRepositoryImpl;
import com.sdacademy.pets.PetsRepository;
import com.sdacademy.veterinarian.VeterinarianEntity;
import com.sdacademy.veterinarian.VeterinarianRepositoryImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;


public class Application {


    public void startMenu() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("1. Menu owner");
            System.out.println("2. Menu pet");
            System.out.println("3. Menu consult");
            System.out.println("4. Menu veterinarian");
            System.out.println("0. Exit");
            int selectedOption = scanner.nextInt();

            switch (selectedOption) {
                case 1:
                    System.out.println("1. Add owner:");
                    System.out.println("2. Update owner:");
                    System.out.println("3. Delete owner:");
                    System.out.println("4. Get all owners:");

                    int ownerOption = scanner.nextInt();

                    switch (ownerOption) {
                        case 1:
                            readAndInsertOwnerDetails();
                            break;
                        case 2:
                            updateOwner();
                            break;
                        case 3:
                            deleteOwner();
                            break;
                        case 4:
                            getAllOwners();
                            break;
                        default:
                            System.out.println("Option not available");
                    }
                    break;
                case 2:
                    System.out.println("1. Add pet:");
                    System.out.println("2. Update pet:");
                    System.out.println("3. Delete pet:");
                    System.out.println("4. Get all pets:");

                    int petOption = scanner.nextInt();

                    switch (petOption) {
                        case 1:
                            readAndInsertPetDetails();
                            break;
                        case 2:
                            updatePet();
                            break;
                        case 3:
                            deletePet();
                            break;
                        case 4:
                            getAllPets();
                            break;
                        default:
                            System.out.println("Option not available");
                    }
                    break;
                case 3:
                    System.out.println("1. Add consult:");
                    System.out.println("2. Update consult:");
                    System.out.println("3. Delete consult:");
                    System.out.println("4. Get all consults:");

                    int consultOption = scanner.nextInt();

                    switch (consultOption) {
                        case 1:
                            readAndInsertConsultDetails();
                            break;
                        case 2:
                            //updateConsults();
                            break;
                        case 3:
                            //deleteConsults();
                            break;
                        case 4:
                            //getAllConsults();
                            break;
                        default:
                            System.out.println("Option not available");
                    }
                    break;
                case 4:
                    System.out.println("1. Add veterinarian:");
                    System.out.println("2. Update veterinarian:");
                    System.out.println("3. Delete veterinarian:");
                    System.out.println("4. Get all veterinarians:");

                    int veterinarianOption = scanner.nextInt();

                    switch (veterinarianOption) {
                        case 1:
                            readAndInsertVeterinarianDetails();
                            break;
                        case 2:
                            //updateVeterinarian();
                            break;
                        case 3:
                            //deleteVeterinarian();
                            break;
                        case 4:
                            //getAllVeterinarians();
                            break;
                        default:
                            System.out.println("Option not available");
                    }
                    break;
                case 0:
                    System.out.println("Thank you for your time!");
                    return;
            }
        }

    }

    /*public void addOwner() {
        try {
            OwnerEntity owner1 = new OwnerEntity();
            owner1.setFirstName("John");
            owner1.setLastName("Popescu");
            owner1.setAdress("London");
            owner1.setPhoneNo("0022171548");
            OwnerEntity owner2 = new OwnerEntity();
            owner2.setFirstName("Michael");
            owner2.setLastName("Jakcson");
            owner2.setAdress("Manchester");
            owner2.setPhoneNo("0022333444");
            OwnerEntity owner3 = new OwnerEntity();
            owner3.setFirstName("Andrew");
            owner3.setLastName("Ion");
            owner3.setAdress("Bucharest");
            owner3.setPhoneNo("002233445");


            Session session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.save(owner3);
            transaction.commit();
            session.close();
        } catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void addPets() {
        try {
            PetsEntity pet1 = new PetsEntity();
            pet1.setPetName("Toto");
            pet1.calculateAge("2014/09/13 00:00");
            pet1.setGender("M");
            pet1.setVaccinated(true);
            pet1.setRace("Colly");

            Session session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.save(pet1);
            transaction.commit();
            session.close();


        } catch (Exception exception) {
            System.out.println(exception);
        }

    }

    public void addVeterinarian() {
        try {
            VeterinarianEntity veterinarian1 = new VeterinarianEntity();
            veterinarian1.setFirstName("Vasile");
            veterinarian1.setLastName("Vasiliu");
            veterinarian1.setAddress("Bucharest");
            veterinarian1.setSpeciality("Nutrition");

            VeterinarianEntity veterinarian2 = new VeterinarianEntity();
            veterinarian2.setFirstName("Ana");
            veterinarian2.setLastName("Ionescu");
            veterinarian2.setAddress("Bucharest");
            veterinarian2.setSpeciality("Surgery");

            Session session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.save(veterinarian1);
            session.save(veterinarian2);

            transaction.commit();
            session.close();

        } catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void addConsult() {
        try {
            ConsultEntity consultEntity1 = new ConsultEntity();
            consultEntity1.setVisitDate(new Date(2020 - 02 - 21));
            consultEntity1.setDescription("vaccinated");
            consultEntity1.setPrice(150.00);

            Session session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.save(consultEntity1);

            transaction.commit();
            session.close();

        } catch (Exception exception) {
            System.out.println(exception);

        }
    }*/

    // Owner menu metods: create, read, update, delet. //////////////////////////////////////////
    public void readAndInsertOwnerDetails() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter First Name: ");
        String firstName = scanner.nextLine();
        System.out.println("Enter Last Name: ");
        String lastName = scanner.nextLine();
        System.out.println("Enter Address: ");
        String address = scanner.nextLine();
        System.out.println("Enter Phone Number: ");
        String phoneNo = scanner.nextLine();
        scanner.close();

        OwnerEntity owner = new OwnerEntity();
        owner.setFirstName(firstName);
        owner.setLastName(lastName);
        owner.setAdress(address);
        owner.setPhoneNo(phoneNo);

        OwnerEntityRepositoryImpl repository = new OwnerEntityRepositoryImpl();
        repository.create(owner);
    }

    public void updateOwner() {
        OwnerRepository ownerRepository = new OwnerEntityRepositoryImpl();
        List<OwnerEntity> ownerEntityList = ownerRepository.findAll();
        for (int i = 0; i < ownerEntityList.size(); i++) {
            System.out.println(ownerEntityList.get(i).getFirstName());
        }
        Scanner scanner = new Scanner(System.in);
        int selectedOwnerPosition = scanner.nextInt() - 1;
        OwnerEntity selectedOwner = ownerEntityList.get(selectedOwnerPosition);
        scanner.nextLine();
        System.out.println("Enter First Name: ");
        String firstName = scanner.nextLine();
        System.out.println("Enter Last Name: ");
        String lastName = scanner.nextLine();
        System.out.println("Enter Address: ");
        String address = scanner.nextLine();
        System.out.println("Enter Phone Number: ");
        String phoneNo = scanner.nextLine();
        scanner.close();

        selectedOwner.setFirstName(firstName);
        selectedOwner.setLastName(lastName);
        selectedOwner.setAdress(address);
        selectedOwner.setPhoneNo(phoneNo);

        ownerRepository.update(selectedOwner.getOwnerId(), selectedOwner);
    }

    public void deleteOwner() {
        OwnerRepository ownerRepository = new OwnerEntityRepositoryImpl();
        List<OwnerEntity> ownerEntityList = ownerRepository.findAll();
        for (int i = 0; i < ownerEntityList.size(); i++) {
            System.out.println(ownerEntityList.get(i).getFirstName());
        }
        Scanner scanner = new Scanner(System.in);
        int selectedOwnerPosition = scanner.nextInt() - 1;

        ownerRepository.delete(ownerEntityList.get(selectedOwnerPosition));
    }

    public void getAllOwners() {
        OwnerRepository ownerRepository = new OwnerEntityRepositoryImpl();
        List<OwnerEntity> ownerEntityList = ownerRepository.findAll();
        for (int i = 0; i < ownerEntityList.size(); i++) {
            System.out.println(ownerEntityList.get(i).toString());
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    //Pet menu metods: create, read, update, delet.///////////////////////////////////

    public void readAndInsertPetDetails() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter pet name:");
        String name = scanner.nextLine();
        System.out.println("Enter pet race:");
        String race = scanner.nextLine();
        System.out.println("Enter pet gender:");
        String gender = scanner.nextLine();
        System.out.println("Enter pet birthdate: ");
        String birthdate = scanner.nextLine();
        System.out.println("Is the pet vaccinated (y/n):");
        Boolean isVaccinated = scanner.nextLine().equals("y");

        PetsEntity pet = new PetsEntity();
        pet.setPetName(name);
        pet.setRace(race);
        pet.setGender(gender);
        pet.calculateAge(birthdate);
        pet.setVaccinated(isVaccinated);
        OwnerEntityRepositoryImpl ownerEntityRepository = new OwnerEntityRepositoryImpl();
        List<OwnerEntity> ownerEntityList = ownerEntityRepository.findAll();
        System.out.println("Select the owner from list:");
        for (OwnerEntity owner : ownerEntityList) {
            System.out.println(owner.getFirstName());
        }
        int ownerPosition = scanner.nextInt();
        pet.setOwnerId(ownerEntityList.get(ownerPosition - 1));

        PetsEntityRepositoryImpl entityRepository = new PetsEntityRepositoryImpl();
        entityRepository.create(pet);

        scanner.close();
    }

    public void updatePet() {
        PetsRepository petsRepository = new PetsEntityRepositoryImpl();
        List<PetsEntity> petsEntityList = petsRepository.findAll();
        for (int i = 0; i < petsEntityList.size(); i++) {
            System.out.println(petsEntityList.get(i).getPetName());
        }
        Scanner scanner = new Scanner(System.in);
        int selectedPetPosition = scanner.nextInt() - 1;
        PetsEntity selectedPet = petsEntityList.get(selectedPetPosition);
        scanner.nextLine();
        System.out.println("Enter pet name: ");
        String petName = scanner.nextLine();
        System.out.println("Enter race: ");
        String race = scanner.nextLine();
        System.out.println("Enter is vaccinated y/n: ");
        Boolean isVaccinated = scanner.nextLine().equals("y");
        System.out.println("Enter gender: ");
        String gender = scanner.nextLine();
        System.out.println("Enter birthdate: ");
        String birthdate = scanner.nextLine();
        scanner.close();

        selectedPet.setPetName(petName);
        selectedPet.setRace(race);
        selectedPet.setVaccinated(isVaccinated);
        selectedPet.setGender(gender);
        selectedPet.calculateAge(birthdate);

        petsRepository.update(selectedPet.getPetId(), selectedPet);
    }

    public void deletePet() {
        PetsRepository petsRepository = new PetsEntityRepositoryImpl();
        List<PetsEntity> petsEntityList = petsRepository.findAll();
        for (int i = 0; i < petsEntityList.size(); i++) {
            System.out.println(petsEntityList.get(i).getPetName());
        }
        Scanner scanner = new Scanner(System.in);
        int selectedOwnerPosition = scanner.nextInt() - 1;

        petsRepository.delete(petsEntityList.get(selectedOwnerPosition));
    }

    public void getAllPets() {
        PetsRepository petsRepository = new PetsEntityRepositoryImpl();
        List<PetsEntity> petEntityList = petsRepository.findAll();
        for (int i = 0; i < petEntityList.size(); i++) {
            System.out.println(petEntityList.get(i).toString());
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    //Veterinarian menu metods: create, read, update, delet./////////////////////////

    public void readAndInsertVeterinarianDetails() {


        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter First Name: ");
        String firstName = scanner.nextLine();
        System.out.println("Enter Last Name: ");
        String lastName = scanner.nextLine();
        System.out.println("Enter Address: ");
        String address = scanner.nextLine();
        System.out.println("Enter Speciality: ");
        String speciality = scanner.nextLine();
        scanner.close();

        VeterinarianEntity veterinarian = new VeterinarianEntity();
        veterinarian.setFirstName(firstName);
        veterinarian.setLastName(lastName);
        veterinarian.setAddress(address);
        veterinarian.setSpeciality(speciality);

        VeterinarianRepositoryImpl repository = new VeterinarianRepositoryImpl();
        repository.create(veterinarian);



    }

    /////////////////////////////////////////////////////////////////////////////////
    //Consult menu metods: create, read, update, delet./////////////////////////

    public void readAndInsertConsultDetails() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter description: ");
            String description = scanner.nextLine();
            System.out.println("Enter price: ");
            Integer price = scanner.nextInt();
            System.out.println("Enter visit date: ");
            String visitDate = scanner.next();
            DateFormat formatter = new SimpleDateFormat("yyyy/mm/dd");
            Date date = formatter.parse(visitDate);

            ConsultEntity consult = new ConsultEntity();
            consult.setVisitDate(date);
            consult.setDescription(description);
            consult.setPrice(price);


            PetsEntityRepositoryImpl petsEntityRepository = new PetsEntityRepositoryImpl();
            List<PetsEntity> petsEntityList = petsEntityRepository.findAll();
            System.out.println("Select the pet from list:");
            for (PetsEntity pet : petsEntityList) {
                System.out.println(pet.getPetName());
            }
            int petsPosition = scanner.nextInt();
            consult.setPetId(petsEntityList.get(petsPosition - 1));

            //////////////////////////////////////////////////////

            VeterinarianRepositoryImpl veterinarianEntityRepository = new VeterinarianRepositoryImpl();
            List<VeterinarianEntity> veterinarianEntityList = veterinarianEntityRepository.findAll();
            System.out.println("Select the veterinarian from list:");
            for (VeterinarianEntity veterinarian : veterinarianEntityList) {
                System.out.println(veterinarian.getFirstName());
            }
            int veterinarianPosition = scanner.nextInt();
            consult.setVeterinaryId(veterinarianEntityList.get(veterinarianPosition - 1));

            ConsultEntityRepositoryImpl consultEntityRepository = new ConsultEntityRepositoryImpl();
            consultEntityRepository.create(consult);

            scanner.close();
        }catch (Exception e){
            System.out.println(e);
        }

    }

}



