package com.sdacademy.veterinarian;

import java.util.List;

public interface VeterinarianRepository {
    void create(VeterinarianEntity veterinarian);

    List<VeterinarianEntity> findAll();

    VeterinarianEntity findById(Integer id);

    void update(VeterinarianEntity oldVeterinarian, VeterinarianEntity newVeterinarian);

    void delete(Integer id);
}
