package com.sdacademy.veterinarian;

import com.sdacademy.consult.ConsultEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "veterinarian")

public class VeterinarianEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer veterinaryId;

    private String firstName;
    private String lastName;
    private String address;
    private String speciality;

    @OneToMany(mappedBy = "veterinaryId")
    private List<ConsultEntity> consultEntities;

    public VeterinarianEntity() {
    }

    public Integer getVeterinaryId() {
        return veterinaryId;
    }

    public void setVeterinaryId(Integer veterinaryId) {
        this.veterinaryId = veterinaryId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {

        return "VeterinarianEntity{" +
                "veterinaryId=" + veterinaryId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", speciality='" + speciality + '\'' +
                '}';
    }

}
