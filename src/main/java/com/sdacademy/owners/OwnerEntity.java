package com.sdacademy.owners;

import com.sdacademy.invoices.InvoicesEntity;
import com.sdacademy.pets.PetsEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "owner")
public class OwnerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ownerId;

    @OneToMany (mappedBy = "ownerId", fetch = FetchType.EAGER)
    private List <PetsEntity> pets;



    private String firstName;
    private String lastName;
    private String address;
    private String phoneNo;


    @OneToMany (mappedBy = "owner")
    private List<InvoicesEntity> invoices;



    public OwnerEntity() {
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAdress() {
        return address;
    }

    public void setAdress(String adress) {
        this.address = adress;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {
        return "OwnerEntity{" +
                "ownerId=" + ownerId +

                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", adress='" + address + '\'' +
                ", phoneNo='" + phoneNo + '\'' +

                '}';
    }
}
