package com.sdacademy.pets;


import java.util.List;

public interface PetsRepository {
    void create (PetsEntity pet);
    PetsEntity findById (Integer petId);
    void update (Integer petId, PetsEntity pet);
    void delete (PetsEntity petId);
    List<PetsEntity> findAll();
}
