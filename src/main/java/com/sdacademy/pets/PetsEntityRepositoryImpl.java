package com.sdacademy.pets;

import com.sdacademy.Connection;
import com.sdacademy.owners.OwnerEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class PetsEntityRepositoryImpl implements PetsRepository {


    @Override
    public void create(PetsEntity pet) {
        Session session = null;
        try {
            session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.save(pet);
            transaction.commit();
        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    @Override
    public void delete(PetsEntity petId) {
        Session session = null;
        try {
            session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.delete(petId);
            transaction.commit();

        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public List<PetsEntity> findAll() {
        Session session = null;
        List<PetsEntity> pets = new ArrayList<>();
        try {
            session = Connection.getInstance().getSession();
            pets = session.createQuery("from PetsEntity", PetsEntity.class).list();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return pets;
    }

    @Override
    public PetsEntity findById(Integer petId) {
        return null;
    }

    @Override
    public void update(Integer petId, PetsEntity pet) {
        Session session = null;
        pet.setPetId(petId);
        try {
            session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.update(pet);
            transaction.commit();

        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            if (session != null)
                session.close();
        }
    }

}
