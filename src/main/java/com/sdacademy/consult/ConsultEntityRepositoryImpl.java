package com.sdacademy.consult;

import com.sdacademy.Connection;
import com.sdacademy.owners.OwnerEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ConsultEntityRepositoryImpl implements ConsultRepository{

    @Override
    public void create(ConsultEntity consult) {
        Session session = null;
        try {
            session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.save(consult);
            transaction.commit();

        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public ConsultEntity findById(Integer visitId) {
        return null;
    }

    @Override
    public void update(Integer visitId, ConsultEntity consult) {
        Session session = null;
        consult.setVisitId(visitId);
        try {
            session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.update(consult);
            transaction.commit();

        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public void delete(Integer visitId) {
        Session session = null;
        try {
            session = Connection.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            session.delete(visitId);
            transaction.commit();

        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public List<ConsultEntity> findAll() {
        Session session = null;
        List<ConsultEntity> consult = new ArrayList<>();
        try {
            session = Connection.getInstance().getSession();
            consult = session.createQuery("from ConsultEntity", ConsultEntity.class).list();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return consult;
    }


}
