package com.sdacademy.consult;

import com.sdacademy.owners.OwnerEntity;

import java.util.List;

public interface ConsultRepository {

    void create(ConsultEntity consult);

    ConsultEntity findById(Integer visitId);

    List<ConsultEntity> findAll();

    void update(Integer visitId, ConsultEntity consult);

    void delete(Integer visitId);
}
