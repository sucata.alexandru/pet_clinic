package com.sdacademy.consult;

import com.sdacademy.invoices.InvoicesEntity;
import com.sdacademy.pets.PetsEntity;
import com.sdacademy.veterinarian.VeterinarianEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "consult")

public class ConsultEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer visitId;

    @ManyToOne
    @JoinColumn(name = "petId")
    private PetsEntity petId;

    @ManyToOne
    @JoinColumn(name = "veterinaryId")
    private VeterinarianEntity veterinaryId;

    private Date visitDate;
    private String description;
    private Integer price;

    @ManyToOne
    @JoinColumn(name = "invoiceNo")
    private InvoicesEntity invoice;


    public ConsultEntity() {
    }

    public Integer getVisitId() {
        return visitId;
    }

    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public PetsEntity getPetId() {
        return petId;
    }

    public void setPetId(PetsEntity petId) {
        this.petId = petId;
    }

    public VeterinarianEntity getVeterinaryId() {
        return veterinaryId;
    }

    public void setVeterinaryId(VeterinarianEntity veterinaryId) {
        this.veterinaryId = veterinaryId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}