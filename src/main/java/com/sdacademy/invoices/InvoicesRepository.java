package com.sdacademy.invoices;

import com.sdacademy.owners.OwnerEntity;

public interface InvoicesRepository {
    void create (InvoicesEntity invoice);
    InvoicesEntity findById (Integer invoiceNo);
    void update (Integer invoiceNo, InvoicesEntity invoice);
    void delete (Integer invoiceNo);
}
