package com.sdacademy.invoices;

import com.sdacademy.consult.ConsultEntity;
import com.sdacademy.owners.OwnerEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "invoice")
public class InvoicesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer invoiceNo;
    private Date date;

    @ManyToOne
    @JoinColumn(name = "ownerId")
    private OwnerEntity owner;

    @OneToMany(mappedBy = "visitId")
    private List<ConsultEntity> consults;


    public InvoicesEntity() {
    }

    public Integer getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Integer invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<ConsultEntity> getConsults() {
        return consults;
    }

    public void setConsults(List<ConsultEntity> consults) {
        this.consults = consults;
    }

    public OwnerEntity getOwner() {
        return owner;
    }

    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

}
